var express = require("express");

var path = require("path");
var sqlite3 = require('sqlite3').verbose();
var crypto = require('crypto');
var cookieParser = require('cookie-parser');
const { copyFileSync } = require("fs");
const { json } = require("body-parser");

var router = express.Router();
var db = new sqlite3.Database(path.join(__dirname, "../web/assets/db/data.db"));
db.run("CREATE TABLE IF NOT EXISTS emp(email TEXT NOT NULL PRIMARY KEY, pass TEXT, hash TEXT);");
db.run("CREATE TABLE IF NOT EXISTS orders(trackID TEXT PRIMARY_KEY, stuff TEXT, status INTEGER, clerk TEXT)");

var adminuser="administrator@localhost";
var unencadminpass=crypto.createHash('md5').update(crypto.randomBytes(16)).digest("hex");
var adminpass=crypto.createHash('sha512').update(unencadminpass).digest("hex");
var adminhash=crypto.createHash('sha512').update(adminuser+";"+adminpass).digest("hex");

// change after finish to "insert or replace"
db.run("INSERT OR REPLACE INTO emp(email, pass, hash) VALUES ('"+adminuser+"','"+adminpass+"','"+adminhash+"');");

console.log("Administrator-Username: " + adminuser);
console.log("Administrator-Password: " + unencadminpass);

const pages = "../../../views/pages/"

router.get("/", function(req,res){
    res.render(pages + "index");
});

// Tracking Page

router.get("/track",function(req,res) {
    res.redirect("/");
});

router.post("/track",function(req,res) {
    if(req.body.trackingID === "") {
        res.redirect("/");
    } else {
        res.redirect("/track/"+req.body.trackingID);
    }
});

router.get("/track/:trackingID",function(req,res) {
    db.serialize(() => {
        var trackingID = req.params.trackingID;
    
        db.get("SELECT * FROM orders WHERE trackID = ?", [trackingID], function(err,rowv) {
        if(err) {
            console.error(err.message);
        }
        if(rowv === undefined) {
            res.redirect("/track");
        } else {
            if(rowv.stuff == null) {
                return res.redirect("/track");
            }
            const objectToArray = obj => {
            const res = [];
            obj.forEach(e => {
                const keys = Object.keys(e);
                const sub = {};
                for(let i = 0; i < keys.length; i++) {
                    sub[keys[i]] = e[keys[i]];
                };
                res.push(sub);
            });
            return res;
            };
            var order = objectToArray(JSON.parse(rowv.stuff));
            res.render(pages + "track", {data:rowv,stuff:order})
            }
        });
    });
});

// Admin Page

router.get("/admin",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                db.serialize(() =>{
                    db.all("SELECT * FROM orders WHERE clerk = ?", [row.email], function(err,dbdata) {
                        if(err) {
                            console.error(err.message);
                        }
                        if(dbdata !== undefined) {
                            return res.render(pages + "admin/admin",{email:row.email,data:dbdata});
                        } else {
                            var dbdata = "";
                            return res.render(pages + "admin/admin",{email:row.email,data:dbdata});
                        }
                    });
                });

                /*
                Array must look like this!
                    [
                        {
                            "Type": "Mainboard",
                            "Brand": "Asus",
                            "Model": "STRIX GAMING",
                            "Status": 0
                        },
                        {
                            "Type": "CPU",
                            "Brand": "AMD",
                            "Model": "Ryzen 9 5900X",
                            "Status": 0
                        }
                    ]

                    use with; Array.push({
                        "Type": "RAM",
                        "Brand": "Corsair",
                        "Model": "HyperX Fury Black",
                        "Status": 0
                    });
                */
            }
        });
    });
});

router.get("/admin/details/:trackID",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                var trackID = req.params.trackID;
                db.get("SELECT * FROM orders WHERE trackID = ?", [trackID], function(err,rowv) {
                    if(err) {
                        console.error(err.message);
                    }
                    if(rowv === undefined) {
                        res.redirect("/admin");
                    } else {
                        if(rowv.stuff == null) {
                            return res.redirect("/admin");
                        }
                        const objectToArray = obj => {
                            const res = [];
                            obj.forEach(e => {
                                const keys = Object.keys(e);
                                const sub = {};
                                for(let i = 0; i < keys.length; i++) {
                                    sub[keys[i]] = e[keys[i]];
                                };
                                res.push(sub);
                            });
                            return res;
                        };

                        var order = objectToArray(JSON.parse(rowv.stuff));
                        res.render(pages + "admin/details", {email:row.email,data:rowv,stuff:order})
                    }
                });
            }
        });
    });
});

router.post("/admin/updateOrder",function(req,res) {
    db.serialize(() => {

        var trackID = req.body.trackID;

        db.get("SELECT * FROM orders WHERE trackID = ?", [trackID], function(err,rowv) {
            if(err) {
                console.error(err.message);
            }
            if(rowv === undefined) {
                res.redirect("/admin");
            } else {
                if(rowv.stuff == null) {
                    return res.redirect("/admin");
                }
                const objectToArray = obj => {
                    const res = [];
                    obj.forEach(e => {
                        const keys = Object.keys(e);
                        const sub = {};
                        for(let i = 0; i < keys.length; i++) {
                            sub[keys[i]] = e[keys[i]];
                        };
                        res.push(sub);
                    });
                    return res;
                };

                var order = objectToArray(JSON.parse(rowv.stuff));
                
                neworder = [];
                order.forEach(el => {
                    neworder.push({
                        "Type": eval("req.body.inputType"+el.Type),
                        "Brand": eval("req.body.inputBrand"+el.Type),
                        "Model": eval("req.body.inputModel"+el.Type),
                        "Status": eval("req.body.select"+el.Type)
                    });
                });

                db.run('UPDATE orders SET stuff = ?, status = ? WHERE trackID = ?;',JSON.stringify(neworder),req.body.selectStatus,trackID);
            }
        });

        res.redirect("/admin");
    });
});

router.get("/admin/orders/add",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                db.serialize(() => {
                    db.all("SELECT email FROM emp;", function(err,val) {
                        if(err) {
                            return console.error(err.message);
                        }
                        res.render(pages + "admin/orders/add",{email:row.email,row:val});
                    });
                });
            }
        });
    });
});

router.post("/admin/orders/add",function(req,res) {
    db.serialize(() => {

        var today = new Date();
     
        var trackID = crypto.createHash('md5').update(today.toString()).digest("hex");
        var count = req.body.count;
        var clerk = req.body.email;

        neworder = [];
        for(var i = 0; i<count;i++) {
            neworder.push({
                "Type": eval("req.body.inputType"+eval(i+1)),
                "Brand": eval("req.body.inputBrand"+eval(i+1)),
                "Model": eval("req.body.inputModel"+eval(i+1)),
                "Status": "0"
            });
        }

        db.run('INSERT INTO orders (trackID,stuff,status,clerk) VALUES (?,?,?,?);',trackID,JSON.stringify(neworder),0,clerk);

        res.redirect("/admin");
    });
});

router.get("/admin/orders/delete/:trackID",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                var trackID = req.params.trackID;
                res.render(pages + "admin/orders/delete",{trackID:trackID,email:row.email});
            }
        });
    });
});

router.post("/admin/orders/delete",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                var trackID = req.body.trackID;
                db.run("DELETE FROM orders WHERE trackID = ?", [trackID], function(err){
                    if(err) {
                        return console.error(err.message);
                    }
                });
                return res.redirect("/admin");
            }
        });
    });
});

router.get("/admin/users",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                db.serialize(() => {
                    db.all("SELECT email FROM emp;", function(err,val) {
                        if(err) {
                            return console.error(err.message);
                        }
                        res.render(pages + "admin/users",{email:row.email,row:val});
                    });
                });
            }
        });
    });
});

router.get("/admin/users/add",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                var data = "";
                res.render(pages + "admin/add", {email:row.email, data:data});
            }
        });
    });
});

router.post("/admin/users/add",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                db.serialize(() => {
                    db.get("SELECT email FROM emp WHERE email = ?", [req.body.email], function(err, rowv) {
                        if(err) {
                            return console.error(err.message);
                        }
                        if(rowv === undefined) {

                            var data = {
                                "email": req.body.email,
                                "email_err": "",
                                "password": req.body.password,
                                "password_err": "",
                                "passwordRepeat": req.body.passwordRepeat,
                                "passwordRepeat_err": ""
                            };

                            if(data["email"] === "") {
                                data["email_err"] = "Empty E-Mail";
                            }

                            if(data["password"] === "" || data["passwordRepeat"] === "") {
                                data["password_err"] = "Passwords do not match!";
                                data["passwordRepeat_err"] = "Passwords do not match!";
                            }

                            if(data["password"] != data["passwordRepeat"]) {
                                data["password_err"] = "Passwords do not match!";
                                data["passwordRepeat_err"] = "Passwords do not match!";
                            }

                            if(data["email_err"] === "" && data["password_err"] === "" && data["passwordRepeat_err"] === "") {
                                var user = data.email;
                                var pass = crypto.createHash('sha512').update(data.password).digest("hex");
                                var hash = crypto.createHash('sha512').update(user+";"+pass).digest("hex");

                                db.run("INSERT INTO emp (email,pass,hash) VALUES ('"+user+"','"+pass+"','"+hash+"')");
                                return res.redirect("/admin/users");
                            } else {
                                return res.render(pages + "admin/add", {data:data,email:row.email});
                            }
                        } else {
                            var data = {
                                "email": "",
                                "email_err": "User exists already!",
                                "password": "",
                                "password_err": "",
                                "passwordRepeat": "",
                                "passwordRepeat_err": ""
                            }
                            return res.render(pages + "admin/add", {data:data,email:row.email});
                        }
                    });
                });
            }
        });
    });
});

router.get("/admin/users/modify/:emailAddress",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                var emailAddress = req.params.emailAddress;
                if(emailAddress == "administrator@localhost") {
                    res.redirect("/admin/users")
                }
                var data = "";
                res.render(pages + "admin/modify",{emailAddress:emailAddress,email:row.email,data:data});
            }
        });
    });
});

router.post("/admin/users/modify",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                var data = {
                    oldEmail: req.body.oldEmail,
                    email: req.body.emailAddress,
                    email_err: "",
                    password: req.body.password,
                    password_err: "",
                    passwordRepeat: req.body.passwordRepeat,
                    passwordRepeat_err: ""
                }

                // Check if user exists already in user registry.
                db.serialize(() => {
                    db.get("SELECT email FROM emp WHERE email = ?", [data.email], function(err,rowv){
                        if(err) {
                            console.error(err.message);
                        }

                        if(rowv !== undefined) {
                            data.email_err = "User already exists!";
                        }
                    });
                });

                // Validating all other errors.
                if(data.email == "") {
                    data.email_err = "Please enter an E-Mail Address";
                }

                if(data.password == "" && data.passwordRepeat == "") {
                    data.password_err = "Please enter a valid password!";
                    data.passwordRepeat_err = "Please enter a valid password!";
                }

                if((data.password != "" && data.passwordRepeat == "") || (data.password == "" && data.passwordRepeat != "") || (data.password != data.passwordRepeat)) {
                    data.password_err = "Passwords do not match!";
                    data.passwordRepeat_err = "Passwords do not match!";
                }

                if(data.email_err == "" && data.password_err == "" && data.passwordRepeat_err == "") {

                    if(data.password == "" && data.passwordRepeat == "") {
                        db.serialize(() => {
                            db.get("SELECT email,pass FROM emp WHERE email = ?", [data.oldEmail], function(err, rowvw) {
                                if(err) {
                                    return console.error(err.message);
                                }

                                if(rowvw !== undefined) {
                                    var password = rowvw.pass;
                                    var hash = crypto.createHash('sha512').update(data.email+";"+password).digest("hex");
                                    db.run("UPDATE emp SET email = ?,hash = ? WHERE email = ?", [data.email,hash,data.oldEmail],function(err) {
                                        if(err) {
                                            return console.error(err.message);
                                        }
                                    });
                                }
                            });
                        });
                    } else {
                        var hashedpassword = crypto.createHash('sha512').update(data.password).digest("hex");
                        var hash = crypto.createHash('sha512').update(data.email+";"+hashedpassword).digest("hex");

                        db.run("UPDATE emp SET email = ?,pass = ?,hash = ? WHERE email = ?", [data.email,hashedpassword,hash,data.oldEmail],function(err) {
                            if(err) {
                                return console.error(err.message);
                            }
                        });
                    }
                    return res.redirect("/admin/users");
                } else {
                    return res.render(pages + "admin/modify",{email:row.email,data:data});
                }
            }
        });
    });
});

router.get("/admin/users/remove/:emailAddress",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                var emailAddress = req.params.emailAddress;
                if(emailAddress == "administrator@localhost") {
                    res.redirect("/admin/users")
                }
                res.render(pages + "admin/remove",{emailAddress:emailAddress,email:row.email});
            }
        });
    });
});

router.post("/admin/users/remove",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                res.redirect("/login");
            } else {
                var emailAddress = req.body.emailAddress;
                if(emailAddress == "administrator@localhost") {
                    return res.redirect("/admin/users");
                }
                db.run("DELETE FROM emp WHERE email = ?", [emailAddress], function(err){
                    if(err) {
                        return console.error(err.message);
                    }
                });
                return res.redirect("/admin/users");
            }
        });
    });
});

router.get("/logout",function(req,res) {
    res.clearCookie('LoginHash');
    res.cookie('LoginHash', '0', { maxAge: 0 });
    res.redirect("/");
});


// Login Page

router.get("/login",function(req,res) {
    db.serialize(() => {
        db.get("SELECT email,hash FROM emp WHERE hash = ?", [req.cookies.LoginHash], function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                let token;
                if(req.cookies.CSRFID === undefined) {
                    currentDate = new Date();
                    time = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();
                    salt = crypto.randomBytes(16);

                    token = crypto.createHash('sha512').update(time + ":" + salt).digest("hex");

                    res.cookie("CSRFID",token);
                } else {
                    token = req.cookies.CSRFID
                }

                res.render(pages + "login",{token:token});
            } else {
                res.redirect("/admin");
            }
        });
    });
});

router.post("/login",function(req,res){

    var data = {
        email: req.body.email,
        email_err: "",
        password: crypto.createHash('sha512').update(req.body.password).digest("hex"),
        password_err: "",
        token: req.body.token,
        token_err: ""
    }

    if(data.email === "" && req.body.password === "") {
        data.email_err = "Please enter a valid e-mail address";
        data.password_err = "Please enter a valid password";
        res.render(pages + "login",{email_err:data.email_err,password_err:data.password_err,token:data.token});
        return;
    }

    if(data.email === "") {
        data.email_err = "Please enter a valid e-mail address";
        res.render(pages + "login",{email_err:data.email_err,token:data.token});
        return;
    }
    if(data.password === "") {
        data.password_err = "Please enter a valid password";
        res.render(pages + "login",{email:data.email,password_err:data.password_err,token:data.token});
        return;
    }
    if(data.token != req.cookies.CSRFID) {
        data.email_err = "Something went wrong, please try again!";
        data.password_err = "Something went wrong, please try again!";
        res.render(pages + "login",{email_err:data.email_err,password_err:data.password_err,token:data.token});
        return;
    }

    db.serialize(()=>{
        db.get("SELECT email,pass,hash FROM emp WHERE email = ?", [data.email],function(err,row) {
            if(err) {
                return console.error(err.message);
            }
            if(row === undefined) {
                data.email_err = "Please enter a valid e-mail address";
                data.password_err = "Please enter a valid password";
                res.render(pages + "login",{email_err:data.email_err,password_err:data.password_err,token:data.token});
            } else {
                if(row.email == data.email && row.pass == data.password) {
                    var hash=crypto.createHash('sha512').update(data.email+";"+data.password).digest("hex");
                    
                    res.cookie("LoginHash",hash);
                    res.redirect("/admin");
                } else {
                    data.password_err = "Please enter a valid password";
                    res.render(pages + "login",{email:data.email,password_err:data.password_err,token:data.token});
                }
            }
        });
    });
});
module.exports = router;