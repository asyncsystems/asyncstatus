var express = require("express");
var path = require("path");
var routes = require("../api/routes");
var favicon = require("serve-favicon");
var bodyParser = require("body-parser");
var cookieParser = require('cookie-parser');

var app = express();
var port = 3000;


app.set("port", port);

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");


app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(routes);
app.use("/assets", express.static(path.join(__dirname, 'assets')));
app.use(favicon(path.join(__dirname, 'assets/img', 'favicon.ico')));

app.listen(port, function() {
    console.log("Server started on port " + port);
});