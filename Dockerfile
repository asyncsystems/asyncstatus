FROM debian:buster
WORKDIR /usr/src/app

RUN apt-get update && apt-get install npm nodejs python -y

COPY package*.json ./

RUN npm ci --only=production
