$(document).ready(() => {
    var addButton = $('#addComponent');
    var wrapper = $('.componentcontainer');

    $(addButton).click(() => {
        var count = wrapper.children().length + 1;
        var fieldHTML = '<div class="card mb-3"><div class="card-body"><div class="float-left"><table><tbody><tr><td><b>Type:</b> <input name="inputType'+count+'" type="text"></td></tr><tr><td><b>Brand:</b> <input name="inputBrand'+count+'" type="text"></td></tr><tr><td><b>Model:</b> <input name="inputModel'+count+'" type="text"></td></tr></tbody></table></div></div></div>';
        $('#countField').val(count);
        $(wrapper).append(fieldHTML);
    });
});